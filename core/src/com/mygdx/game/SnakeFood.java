package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class SnakeFood {

    private Vector2 foodPosition;
    private Texture foodTexture;

    public SnakeFood(int gridSize) {
        foodTexture = new Texture("food.png");
        spawnFood(gridSize);
    }

    public void spawnFood(int gridSize) {
        foodPosition = new Vector2(MathUtils.random(gridSize - 1), MathUtils.random(gridSize - 1));
    }

    public void drawFood(SpriteBatch batch, int tileSize) {
        batch.draw(foodTexture, foodPosition.x * tileSize, foodPosition.y * tileSize, tileSize, tileSize);
    }

    public Vector2 getFoodPosition() {
        return foodPosition;
    }

    public void dispose() {
        foodTexture.dispose();
    }
}
