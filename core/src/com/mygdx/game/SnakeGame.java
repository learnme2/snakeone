package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class SnakeGame extends ApplicationAdapter {

	private OrthographicCamera camera;
	private Array<Vector2> snake;
	private SnakeFood food;
	private SnakeRenderer renderer;
	private SnakeLogic logic;

	public static final int GRID_SIZE = 20;
	public static final int TILE_SIZE = 32;

	@Override
	public void create() {
		float windowWidth = GRID_SIZE * TILE_SIZE;
		float windowHeight = GRID_SIZE * TILE_SIZE;

		camera = new OrthographicCamera();
		camera.setToOrtho(false, windowWidth, windowHeight);
		camera.zoom = 1.0f;

		snake = new Array<>();
		snake.add(new Vector2(5, 5));

		food = new SnakeFood(GRID_SIZE);
		renderer = new SnakeRenderer();
		logic = new SnakeLogic(snake, food, GRID_SIZE);
	}

	@Override
	public void resize(int width, int height) {
		renderer.resize(width, height, camera);
	}

	@Override
	public void render() {
		logic.render();

		Gdx.gl.glClearColor(0, 0.5f, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		renderer.render(camera, snake, food);
	}

	@Override
	public void dispose() {
		renderer.dispose();
		food.dispose();
		logic.dispose();
	}
}
