package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class SnakeLogic {

    private OrthographicCamera camera;
    private Array<Vector2> snake;
    private SnakeFood food;
    private static final int GRID_SIZE = 20;
    private static final int TILE_SIZE = 32; // Rozmiar kafelka
    private SpriteBatch batch;
    private Vector2 snakeDirection = Vector2.X;
    private float moveTimer = 0f;
    private float baseMoveInterval = 0.5f;
    private float speedMultiplier = 0.9f;
    private float moveInterval = baseMoveInterval;
    private int score = 0;
    private BitmapFont scoreFont;
    private Viewport viewport;
    private Texture headTexture;
    private Texture bodyTexture;

    public SnakeLogic(Array<Vector2> snake, SnakeFood food, int gridSize) {
        this.snake = snake;
        this.food = food;

        float windowWidth = gridSize * TILE_SIZE;
        float windowHeight = gridSize * TILE_SIZE;

        camera = new OrthographicCamera();
        camera.setToOrtho(false, windowWidth, windowHeight);
        camera.zoom = 1.0f;

        viewport = new FitViewport(windowWidth, windowHeight, camera);
        viewport.apply();

        headTexture = new Texture("head.png");
        bodyTexture = new Texture("body.png");

        batch = new SpriteBatch();
        scoreFont = new BitmapFont();

    }


    public void render() {
        handleInput();
        update();

        Gdx.gl.glClearColor(0, 0.5f, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        food.drawFood(batch, TILE_SIZE);
        drawSnake();

        scoreFont.draw(batch, "Score: " + score, 10, viewport.getWorldHeight() - 10);

        batch.end();

        if (isOutOfBounds() || selfCollision()) {
            resetGame();
        }
    }

    public void dispose() {
        batch.dispose();
        scoreFont.dispose();
        food.dispose();
    }

    public void update() {
        moveTimer += Gdx.graphics.getDeltaTime();
        moveInterval = baseMoveInterval * (float) Math.pow(speedMultiplier, score);

        if (moveTimer >= moveInterval) {
            moveSnake(snakeDirection);
            moveTimer = 0f;
        }
    }

    private void moveSnake(Vector2 direction) {
        Vector2 newPosition = snake.first().cpy().add(direction);
        snake.insert(0, newPosition);
        if (newPosition.equals(food.getFoodPosition())) {
            food.spawnFood(GRID_SIZE);
            score++;
        } else {
            snake.removeIndex(snake.size - 1);
        }
    }

    public void handleInput() {
        if (Gdx.input.isKeyPressed(Input.Keys.UP) && snakeDirection.y == 0) {
            snakeDirection.set(0, 1);
        }

        if (Gdx.input.isKeyPressed(Input.Keys.DOWN) && snakeDirection.y == 0) {
            snakeDirection.set(0, -1);
        }

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT) && snakeDirection.x == 0) {
            snakeDirection.set(-1, 0);
        }

        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) && snakeDirection.x == 0) {
            snakeDirection.set(1, 0);
        }
    }

    private void drawSnake() {
        batch.setColor(Color.WHITE);
        for (int i = 0; i < snake.size; i++) {
            Texture segmentTexture = (i == 0) ? headTexture : bodyTexture;
            batch.draw(segmentTexture, snake.get(i).x * TILE_SIZE, snake.get(i).y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
        }
    }


    private boolean isOutOfBounds() {
        Vector2 head = snake.first();
        return head.x < 0 || head.x >= GRID_SIZE || head.y < 0 || head.y >= GRID_SIZE;
    }


    private boolean selfCollision() {
        Vector2 head = snake.first();
        for (int i = 1; i < snake.size; i++) {
            if (head.equals(snake.get(i))) {
                return true;
            }
        }
        return false;
    }

    private void resetGame() {
        snake.clear();
        score = 0;
        snake.add(new Vector2(5, 5));
        food.spawnFood(GRID_SIZE);
    }
}
