package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;

import static com.mygdx.game.SnakeGame.TILE_SIZE;

public class SnakeRenderer {

    private SpriteBatch batch;
    private Texture headTexture;
    private Texture bodyTexture;
    private Texture foodTexture;
    private BitmapFont scoreFont;
    private FitViewport viewport;

    public SnakeRenderer() {
        batch = new SpriteBatch();
        headTexture = new Texture("head.png");
        bodyTexture = new Texture("body.png");
        foodTexture = new Texture("food.png");
        scoreFont = new BitmapFont();
        viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        viewport.apply();
    }

    public void resize(int width, int height, OrthographicCamera camera) {
        viewport.update(width, height, true);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();
    }

    public void render(OrthographicCamera camera, Array<Vector2> snake, SnakeFood food) {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        drawFood(food);
        drawSnake(snake);

        scoreFont.draw(batch, "Score: " + snake.size, viewport.getWorldWidth() - 620, viewport.getWorldHeight() + 140);

        batch.end();
    }

    public void dispose() {
        batch.dispose();
        headTexture.dispose();
        bodyTexture.dispose();
        foodTexture.dispose();
        scoreFont.dispose();
    }

    private void drawSnake(Array<Vector2> snake) {
        batch.setColor(Color.WHITE);
        for (int i = 0; i < snake.size; i++) {
            Texture segmentTexture = (i == 0) ? headTexture : bodyTexture;
            batch.draw(segmentTexture, snake.get(i).x * TILE_SIZE, snake.get(i).y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
        }
    }

    private void drawFood(SnakeFood food) {
        batch.draw(foodTexture, food.getFoodPosition().x * TILE_SIZE, food.getFoodPosition().y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
    }
}
